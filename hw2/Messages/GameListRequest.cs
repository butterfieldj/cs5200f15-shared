﻿using System;
using System.Runtime.Serialization;

namespace Messages
{
    [DataContract]
    public class GameListRequest : Request
    {
        [DataMember]
        public int StatusFilter { get; set; }
    }
}
