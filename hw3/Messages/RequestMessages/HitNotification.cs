﻿using System;
using System.Runtime.Serialization;

using SharedObjects;

namespace Messages.RequestMessages
{
    [DataContract]
    public class HitNotification : Request
    {
        [DataMember]
        public int ByPlayerId { get; set; }
    }
}
