﻿using System;
using System.Runtime.Serialization;

namespace Messages.AuctionMessages
{
    [DataContract]
    public class AuctionStatus
    {
        public enum PossibleTypeCodes { Unknown = 0, PriceReduction = 1, Ended = 2, Won = 3 };

        [DataMember]
        public PossibleTypeCodes Type { get; set; }
        [DataMember]
        public int CurrentPrice { get; set; }
        [DataMember]
        public int WinningPlayerId { get; set; }
    }
}
