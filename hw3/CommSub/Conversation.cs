﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using CommSub;
using Messages;
using Messages.RequestMessages;
using Messages.ReplyMessages;
using SharedObjects;

using log4net;
using Utils;

namespace CommSub
{
    public abstract class Conversation
    {
        #region Private and Protected Data Members
        private static readonly ILog log = LogManager.GetLogger(typeof(Conversation));

        protected MessageNumber queueId;
        protected Error error = null;
        protected ProcessInfo remoteProcess = null;
        #endregion

        #region Constructors and Factories
        public Conversation() { }

        public static Conversation Create(Type type)
        {
            return Activator.CreateInstance(type) as Conversation;
        }
        #endregion

        #region Public Properties and Methods
        public delegate void CallbackWithRegistryEntries(List<ProcessInfo> data);
        public delegate void CallbackWithGameInfo(List<GameInfo> data);

        public CommProcessState ProcessState { get; set; }
        public CommSubsystem CommSubsystem { get; set; }
        public Communicator MyCommunicator { get { return (CommSubsystem == null) ? null : CommSubsystem.Communicator; } }
        public Envelope IncomingEnvelope { get; set; }
        public int Timeout { get; set; }
        public int MaxRetries { get; set; }
        public bool Done { get; set; }

        public void Launch(object context = null)
        {
            bool result = ThreadPool.QueueUserWorkItem(Execute, context);
            log.DebugFormat("Launch result = {0}", result);
        }

        public void ExecuteSync(object context = null)
        {
            log.Debug("Execute conversation synchronously");

            Launch(context);

            log.Debug("Launched -- wait for change in status");
            while (!ProcessState.Quit && !Done)
                Thread.Sleep(100);
        }

        public abstract void Execute(object context);
        #endregion

        #region Private and Protected Methods
        protected bool IsEnvelopeValid(Envelope env, bool mustBeKnownProcess, bool mustBeInGame, params Type[] allowedTypes)
        {
            bool result = false;
            error = null;
            log.Debug("Checking to see if envelope is valid and message of appropriate type");
            if (env == null || env.Message == null)
            {
                error = Error.Get(Error.StandardErrorNumbers.NullEnvelopeOrMessage);
                log.Error(error.Message);
            }
            else if (env.Message.MessageNr.ProcessId == 0 && !(env.Message is LoginRequest))
            {
                error = Error.Get(Error.StandardErrorNumbers.LocalProcessIdInMessageNumberCannotBeZero);
                log.Warn(error.Message);
            }
            //else if (mustBeKnownProcess && CommSubsystem.KnownProcesses.FindProcessByEP(env.EP) == null)
            //{
            //    error = Error.Get(Error.StandardErrorNumbers.UnknownEndPoint);
            //    error.Message += string.Format(": EP = {0}", env.EP);
            //    log.Warn(error.Message);
            //}
            else if (mustBeInGame && CommSubsystem.KnownProcesses[env.Message.MessageNr.ProcessId] == null)
            {
                error = Error.Get(Error.StandardErrorNumbers.LocalProcessIdInMessageNumberIsNotAProcessId);
                error.Message += string.Format(": LocalProcessId in MessageNr = {0}", env.Message.MessageNr.ProcessId);
                log.Warn(error.Message);
            }
            else
            {
                Type messageType = env.Message.GetType();
                if (!allowedTypes.Contains(messageType))
                {
                    error = Error.Get(Error.StandardErrorNumbers.InvalidTypeOfMessage);
                    string tmp = string.Empty;
                    foreach (Type t in allowedTypes)
                        tmp += t.ToString();
                    error.Message += ". Allowed type(s): " + tmp;
                    log.Warn(error.Message);
                }
                else
                {
                    result = true;
                    remoteProcess = CommSubsystem.KnownProcesses[env.Message.MessageNr.ProcessId];
                }
            }
            return result;
        }

        protected void UnreliableSend(Envelope envelope)
        {
            if (MyCommunicator!=null && envelope!=null)
                MyCommunicator.Send(envelope);
        }

        #endregion
    }
}
